#!/bin/bash

clean()
{
    brew remove --force --ignore-dependencies $1
    brew cleanup -s $1
    brew cleanup --prune-prefix
}

version=$1
if test "$version" == ""
then
    echo "Error. Missing argument"
    exit 1
fi

if brew ls --versions ${version} > /dev/null
then
    clean ${version}
fi
brew install --overwrite -v --cc=clang --build-from-source brew/${version}.rb
clean ${version}

