FROM ubuntu:22.04

# Installing as root: docker images are usually set up as root.
# Since some autotools scripts might complain about this being unsafe, we set
# FORCE_UNSAFE_CONFIGURE=1 to avoid configure errors.
ENV FORCE_UNSAFE_CONFIGURE=1
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update
RUN apt-get upgrade -y build-essential ca-certificates coreutils curl environment-modules gfortran git gpg lsb-release python3 python3-dev python3-distutils python3-venv unzip zip bash sudo vim

RUN groupadd -f -g 1000 gitlab && \
    useradd -u 1000 -g gitlab -ms /bin/bash gitlab && \
    mkdir /builds && \
    chown -R gitlab:gitlab /builds && \
    chmod g+s /builds && \
    echo "gitlab:gitlab" | chpasswd && adduser gitlab sudo
RUN usermod -aG sudo gitlab
RUN chown -R gitlab:gitlab /home/gitlab/

# install spack in /home/gitlab/
USER gitlab
WORKDIR /home/gitlab/
RUN git clone -c feature.manyFiles=true https://github.com/spack/spack.git && \
    . spack/share/spack/setup-env.sh && \
    spack spec zlib
ENV PATH=/home/gitlab/spack/bin:$PATH
RUN spack install $(spack spec starpu | grep " -       \^" | sed 's/.*\^//' | sed 's/@.*//')

# default working directory is
WORKDIR /builds
